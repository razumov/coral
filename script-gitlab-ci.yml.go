//generate gitlab-ci.yaml for coral and cool:
//go run script-gitlab-ci.yml.go
//mv coral.gitlab-ci.yml .gitlab-ci.yml

package main

import (
	"fmt"
	"io/ioutil"
	"log"
	"regexp"
	"strings"
	"time"
)

//generate gitlab-ci.yaml for coral for python3: -v dev3python3/latest
func coralGitlab(platforms map[string][]string) string {
	//first part of the file
	start := `--- 
variables: 
  LCG_VERSION: 94
stages: 
  - build
  - test

`
	//add accumulates all the build and test parts of all the platforms
	var add string
	for viewName, binaryTags := range platforms {
		for _, binaryTag := range binaryTags {
			re := regexp.MustCompile("(.*?)-(.*?)-")
			tag := re.FindStringSubmatch(binaryTag)[2]
			//build part
			file := `
  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - $BINARY_TAG/
      - logs/
`
			file = file + `  script: "./cc-build -b $BINARY_TAG -v ` + viewName + `/latest"`
			file = file + `
  stage: build
  tags: 
`
			//stringToChange is the part of the file for a given platform
			stringToChange := "build_" + binaryTag + "_" + viewName + ":\n  variables:\n    BINARY_TAG: " + binaryTag + file + "  - " + tag + "\n"

			// test part
			file = `  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - logs/qmtestCoral/$BINARY_TAG
  script: 
    - cd $BINARY_TAG/
    - ./cc-sh
    - "export CORAL_AUTH_PATH=/home/gitlab-runner/"
    - "export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/"
    - "kinit -kt /home/gitlab-runner/keytabcdelort cdelort"
    - "time ./qmtestRun.sh"
    - ../logs/qmtestCoral/summarizeAll.sh
    - "cd ../logs/qmtestCoral/"
    - "mkdir $BINARY_TAG"
    - "cp $BINARY_TAG.summary $BINARY_TAG.xml ./$BINARY_TAG"
    - "! grep FAIL $BINARY_TAG.summary"
  stage: test
  when: always
  tags: 
    - `
			stringToChange += "test_" + binaryTag + "_" + viewName + ":\n  variables:\n    BINARY_TAG: " + binaryTag +
				"\n  dependencies:\n    - build_" + binaryTag + "_" + viewName + "\n" + file + tag + "\n"
			add += stringToChange
		}
	}
	return start + add
}

func coolGitlab(platforms []string) string {
	start := `--- 
variables: 
  LCG_VERSION: 93
stages: 
  - build
  - test

`
	var add string
	for _, binaryTag := range platforms {
		re := regexp.MustCompile("(.*?)-(.*?)-")
		tag := re.FindStringSubmatch(binaryTag)[2]
		//build part
		file := `
  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - $BINARY_TAG/
      - logs/
  before_script:
     - "export highest=` + "`"
		cmd := `curl --header \"PRIVATE-TOKEN: $PRIVATE_TOKEN\" \"https://gitlab.cern.ch/api/v4/projects/25684/jobs\" | jq  \".[] | select(.name == \\\"build_$BINARY_TAG\\\") | .id\" | sort -n | tail -1` + "`\""
		file2 := `
     - "curl --header \"PRIVATE-TOKEN: $PRIVATE_TOKEN\" \"https://gitlab.cern.ch/api/v4/projects/25684/jobs/$highest/artifacts\" > coral-$BINARY_TAG.zip"
     - "unzip coral-$BINARY_TAG.zip -d ./coral"
  script: "./cc-build -b $BINARY_TAG"
  stage: build
  tags: 
`
		stringToChange := "build_" + binaryTag + ":\n  variables:\n    BINARY_TAG: " + binaryTag + file + cmd + file2 + "  - " + tag + "\n"
		// test part
		file = `  artifacts: 
    when: always
    expire_in: "3 mos"
    paths: 
      - logs/qmtestCool/$BINARY_TAG
  script: 
    - cd $BINARY_TAG/
    - ./cc-sh
    - "export CORAL_AUTH_PATH=/home/gitlab-runner/"
    - "export CORAL_DBLOOKUP_PATH=/home/gitlab-runner/"
    - "export COOL_QMTEST_USER=sftnight"
    - "time ./qmtestRun.sh"
    - ../logs/qmtestCool/summarizeAll.sh
    - "cd ../logs/qmtestCool/"
    - "mkdir $BINARY_TAG"
    - "cp $BINARY_TAG.summary $BINARY_TAG.xml ./$BINARY_TAG"
    - "! grep FAIL $BINARY_TAG.summary"
  stage: test
  when: always
  tags: 
    - `
		stringToChange += "test_" + binaryTag + ":\n  variables:\n    BINARY_TAG: " + binaryTag +
			"\n  dependencies:\n    - build_" + binaryTag + "\n" + file + tag + "\n"
		add += stringToChange
	}
	return start + add
}

func main() {

	// TODO add ubuntu
	var platforms = make(map[string][]string)
	var viewName [2]string
	viewName[0] = "dev3"
	viewName[1] = "dev3python3"
	for i := 0; i < len(viewName); i++ {
		fmt.Println(viewName[i])
		files, err := ioutil.ReadDir("/cvmfs/sft.cern.ch/lcg/views/" + viewName[i] + "/latest/")
		if err != nil {
			log.Fatal(err)
		}
		for _, file := range files {
			//take only the ones that are recent, contain x86_64 and not ubuntu
			if file.ModTime().After(time.Now().AddDate(0, 0, -1)) && strings.Contains(file.Name(), "x86_64") && !strings.Contains(file.Name(), "ubuntu") { // true
				platforms[viewName[i]] = append(platforms[viewName[i]], file.Name())
			}
		}
	}
	//print platforms
	// fmt.Printf("%v", platforms)

	err := ioutil.WriteFile("coral.gitlab-ci.yml", []byte(coralGitlab(platforms)), 0644)
	if err != nil {
		panic(err)
	}
	// err = ioutil.WriteFile("cool.gitlab-ci.yml", []byte(coolGitlab(platforms)), 0644)
	// if err != nil {
	// 	panic(err)
	// }

}
