# - Locate libaio library
# Defines:
#
#  LIBAIO_FOUND
#  LIBAIO_LIBRARIES
#  LIBAIO_LIBRARY_DIRS (not cached)

find_library(LIBAIO_LIBRARIES NAMES aio)

get_filename_component(LIBAIO_LIBRARY_DIRS ${LIBAIO_LIBRARIES} PATH)

# handle the QUIETLY and REQUIRED arguments and set LIBAIO_FOUND to TRUE if
# all listed variables are TRUE
INCLUDE(FindPackageHandleStandardArgs)
FIND_PACKAGE_HANDLE_STANDARD_ARGS(Libaio DEFAULT_MSG LIBAIO_LIBRARIES)

mark_as_advanced(LIBAIO_FOUND LIBAIO_LIBRARIES)
