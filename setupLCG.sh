# Set the default LCG_XX release version
lcg_xx=88
lcgrel=

#-------------------------------------------------------------------------------
# Check that this setup script has been sourced
#-------------------------------------------------------------------------------

if [ "$BASH_SOURCE" = "$0" ]; then # See CORALCOOL-2823
  echo "ERROR! The setup script was not sourced?" > /dev/stderr
  exit 1
elif [ "$BASH_SOURCE" = "" ]; then
  echo "ERROR! The setup script was not sourced from bash?" > /dev/stderr
  return 1
fi

# Determine the path to this sourced script
topdir=`dirname ${BASH_SOURCE}`
topdir=`cd ${topdir}; pwd`

#-------------------------------------------------------------------------------
# Parse command line arguments
#-------------------------------------------------------------------------------

usage()
{ 
  echo "Usage:   . $BASH_SOURCE [-b <BINARY_TAG>] [-v <view_name>]" > /dev/stderr
  echo "Example: . $BASH_SOURCE -v dev3/latest"
  echo "Example: . $BASH_SOURCE -v LCG_93"
  echo "Example: . $BASH_SOURCE -v dev4/Thu"
  echo ""
  echo "Use $BASH_SOURCE to set up CORAL/COOL builds from an LCG release/nightly area"
  echo "- set BINARY_TAG from the -b option or the \$BINARY_TAG env variable"
  echo "- prepend paths to \$CMAKE_PREFIX_PATH from the -l or -r options"
  echo "- set up the compiler (set CC and CXX) from the resulting \$BINARY_TAG"
  echo "- set CMAKE_BUILD_TYPE from the resulting \$BINARY_TAG"
  echo "- set CMAKE_CXX_FLAGS from the resulting \$BINARY_TAG"
  echo "- set up ccache and ninja if the executables are found"
}

# Optionally choose an LCG release from command line arguments
# [Default: use the environment variable BINARY_TAG previously set by the user]
if [ "$1" == "-b" ]; then
  if [ "$2" != "" ]; then export BINARY_TAG=$2; shift 2; else usage; return 1; fi
fi

# Require BINARY_TAG to be set a priori (CORALCOOL-2846).
# [Do not set BINARY_TAG from host arch/os/compiler any more (CORALCOOL-2851)]
# [Do not set BINARY_TAG from CMTCONFIG/CMAKECONFIG any more (CORALCOOL-2850)]
if [ "$BINARY_TAG" == "" ]; then
  echo "ERROR! BINARY_TAG is not set" > /dev/stderr
  exit 1
fi
echo "INFO: set up CORAL/COOL for BINARY_TAG ${BINARY_TAG}"

# Optionally choose an LCG release from command line arguments
# [Default: use the variable lcg_xx defined at the top of this script]
if [ "$1" == "-v" ]; then
  if [ "$2" != "" ]; then lcgrel=/cvmfs/sft.cern.ch/lcg/views/${2%/}/${BINARY_TAG}; shift 2; else usage; return 1; fi
else
  lcgrel=/cvmfs/sft.cern.ch/lcg/views/dev3/latest/${BINARY_TAG}
fi

# Fail if unexpected arguments are present
if [ "$*" != "" ]; then
  usage
  return 1
fi

# Sanity check on command line arguments
echo "INFO: set up CORAL/COOL against ${lcgrel}"
if [ ! -d "$lcgrel" ]; then
  echo "ERROR! Directory '${lcgrel}' does not exist" > /dev/stderr
  return 1
fi

#-------------------------------------------------------------------------------
# Set up compliler,PATH and LD_LIBRARY_PATH using views
#-------------------------------------------------------------------------------
source ${lcgrel}/setup.sh 

#-------------------------------------------------------------------------------
# Set up CMAKE_BUILD_TYPE from LCG build type in BINARY_TAG (CORALCOOL-2846)
#-------------------------------------------------------------------------------

# Set CMAKE_BUILD_TYPE from the LCG build type in BINARY_TAG
# [See lcg_get_target_platform in heptools-common.cmake from lcgcmake]
lcgbty=${BINARY_TAG##*-}
if [ "$lcgbty" == "opt" ]; then
  CMAKE_BUILD_TYPE=Release
elif [ "$lcgbty" == "dbg" ]; then
  CMAKE_BUILD_TYPE=Debug
elif [ "$lcgbty" == "cov" ]; then
  CMAKE_BUILD_TYPE=Coverage
elif [ "$lcgbty" == "pro" ]; then
  CMAKE_BUILD_TYPE=Profile
elif [ "$lcgbty" == "o2g" ]; then
  CMAKE_BUILD_TYPE=RelWithDebInfo
elif [ "$lcgbty" == "min" ]; then
  CMAKE_BUILD_TYPE=MinSizeRel
else
  echo "ERROR! Unknown LCG build type: '$lcgbty'" > /dev/stderr
  return 1
fi

#-------------------------------------------------------------------------------
# Set up CMAKE_CXX_FLAGS from BINARY_TAG (CORALCOOL-2854 and CORALCOOL-2846)
#-------------------------------------------------------------------------------

# Build flags (C++ std)
if [[ $BINARY_TAG == *clang* ]]; then
  CMAKE_CXX_FLAGS="-std=c++14"
elif [[ $BINARY_TAG == *gcc6* ]]; then # CORALCOOL-2895 and CORALCOOL-2913
  CMAKE_CXX_FLAGS="-std=c++14"
elif [[ $BINARY_TAG == *gcc5* ]]; then
  CMAKE_CXX_FLAGS="-std=c++14"
fi

# Build flags (gcc-toolchain options for clang)
# [NB These are now no longer passed to lcgcmake (CORALCOOL-2846): is this ok?]
if [[ $BINARY_TAG == *mac* ]]; then
  : # noop
elif [[ $BINARY_TAG == *clang35* ]]; then # See CORALCOOL-2821
  CMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} --gcc-toolchain=/cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.1/x86_64-slc6" # cvmfs instead of AFS, see CORALCOOL-2855
elif [[ $BINARY_TAG == *clang37* ]]; then # See CORALCOOL-2821
  CMAKE_CXX_FLAGS="${CMAKE_CXX_FLAGS} --gcc-toolchain=/cvmfs/sft.cern.ch/lcg/releases/gcc/4.9.3/x86_64-slc6" # cvmfs instead of AFS, see CORALCOOL-2855
fi

#-------------------------------------------------------------------------------
# Set up Boost extra configuration for CMake as in lcgcmake (SPI-842)
#-------------------------------------------------------------------------------

# Follow the same logic as in lcgcmake/cmake/toolchain/heptools-common.cmake 
# See CORALCOOL-2806 (mac), SPI-842 (clang) and CORALCOOL-2797 (icc)
if [[ $BINARY_TAG == *mac* ]]; then
  Boost_COMPILER=" -DBoost_COMPILER=-xgcc42"
elif [[ $BINARY_TAG == *clang* ]]; then
  Boost_COMPILER=${BINARY_TAG%-*}
  Boost_COMPILER=" -DBoost_COMPILER=-${Boost_COMPILER#*-*-}"
elif [[ $BINARY_TAG == *icc* ]]; then
  Boost_COMPILER=" -DBoost_COMPILER=-il"
else
  Boost_COMPILER=
fi

# Always add Boost 1.59 (LCG80-84) and 1.61 (LCG85 and above)
# See CORALCOOL-2806 (1.59) and CORALCOOL-2903 (1.61) 
# Add only the two-digit Boost version as in Pere's lcgcmake patch (SPI-842)
Boost_extra_configuration="-DBoost_NO_BOOST_CMAKE=ON -DBoost_ADDITIONAL_VERSIONS='1.59 1.61'$Boost_COMPILER"

# Configure the ccache directory (CORALCOOL-2844 and CORALCOOL-2846)
# Keep this here to allow interactive queries in this shell
export CCACHE_DIR=$topdir/.ccache
export CCACHE_CONFIGPATH=$CCACHE_DIR/ccache.conf


#-------------------------------------------------------------------------------
# Set up command line options to CMake
#-------------------------------------------------------------------------------

CMAKEFLAGS=

# Use ninja by default, if it exists, in the build rules generated by CMake
# [This is a CMake standard variable]
if ninja --version > /dev/null 2>&1; then 
  CMAKEFLAGS="-GNinja $CMAKEFLAGS"
fi

# Set BINARY_TAG (CORALCOOL-2849)
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DBINARY_TAG=$BINARY_TAG $CMAKEFLAGS"

# Set CMAKE_BUILD_TYPE
# [This is a CMake standard variable]
CMAKEFLAGS="-DCMAKE_BUILD_TYPE=$CMAKE_BUILD_TYPE $CMAKEFLAGS"

# Set CMAKE_CXX_FLAGS
CMAKEFLAGS="-DCMAKE_CXX_FLAGS=$CMAKE_CXX_FLAGS $CMAKEFLAGS"

# Set CMAKE_VERBOSE_MAKEFILE
# ** This is now enabled by default only in lcgcmake and cc-build, not here! **
# [This is a CMake standard variable]
# This sets VERBOSE=1 for make and passes -v as ninja command line argument
# Note that CMake >= 3.3 is required fo this to have an effect on ninja
# See https://github.com/ninja-build/ninja/issues/900
###CMAKEFLAGS="-DCMAKE_VERBOSE_MAKEFILE=ON $CMAKEFLAGS"

# Set Boost_ADDITIONAL_VERSIONS, Boost_COMPILER, Boost_NO_BOOST_CMAKE
# [These are CMake standard variables]
CMAKEFLAGS="$Boost_extra_configuration $CMAKEFLAGS"

# Enable ccache by default
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DCMAKE_USE_CCACHE=ON $CMAKEFLAGS"

# Relocate paths below LCG_releases_base (CORALCOOL-2829)
# [This is a CORAL/COOL variable, not a CMake standard variable]
CMAKEFLAGS="-DLCG_releases_base:STRING=${lcgrel} $CMAKEFLAGS"

# Set up Python3 (CORALCOOL-2976)
# [This is a CORAL/COOL variable, not a CMake standard variable]
if [[ ${lcgrel} =~ "python3" ]]; then
  CMAKEFLAGS="-DLCG_python3=on $CMAKEFLAGS"
  # Also set up Python_ADDITIONAL_VERSIONS
  Python_config_version_twodigit=$(${lcgrel}/bin/python -V | awk '{split($2,a,"."); print a[1]"."a[2]}')
  CMAKEFLAGS="-DPython_ADDITIONAL_VERSIONS=$Python_config_version_twodigit $CMAKEFLAGS"
  CMAKEFLAGS="-DPython_config_version_twodigit=$Python_config_version_twodigit $CMAKEFLAGS"
fi

# Export CMAKEFLAGS for future use in the CORAL/COOL Makefile
# [** NB: This is a CORAL/COOL variable, not a CMake standard variable! **]
export CMAKEFLAGS

#-------------------------------------------------------------------------------
# Set up optional configuration parameters
#-------------------------------------------------------------------------------

# Optionally override installation directory location
# Use absolute paths or paths relative to the _build_ directory!
###export CMAKEFLAGS="-DCMAKE_INSTALL_PREFIX=../my.${BINARY_TAG} $CMAKEFLAGS"
###export CMAKEFLAGS="-DCMAKE_INSTALL_PREFIX=/tmp/my.${BINARY_TAG} $CMAKEFLAGS"

# Add setup overrides if any are present
if [ -f $topdir/overrideSetupCMake.sh ]; then
  echo "INFO: override defaults using $topdir/overrideSetupCMake.sh"
  . $topdir/overrideSetupCMake.sh
else
  echo "INFO: no overrides found"
fi

#-------------------------------------------------------------------------------
# Dump all variables set by the setupLCG.sh script
#-------------------------------------------------------------------------------
echo "Dump all variables set by the setupLCG.sh script"
echo BINARY_TAG=$BINARY_TAG
echo CMAKE_PREFIX_PATH=$CMAKE_PREFIX_PATH
echo CMAKEFLAGS=$CMAKEFLAGS
echo PATH=$PATH
echo CC=$CC
echo CXX=$CXX
echo LCG_release_area=$LCG_release_area
echo LCG_hostos=$LCG_hostos
echo CCACHE_DIR=$CCACHE_DIR
###echo CCACHE_CONFIGPATH=$CCACHE_CONFIGPATH

