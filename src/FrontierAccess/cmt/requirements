package FrontierAccess

#============================================================================
# Private dependencies and build rules
#============================================================================

private

# Temporary hack to avoid c++ deprecation warnings for auto_ptr (bug #98085)
macro_append cppflags "" target-c11 " -Wno-deprecated "

#----------------------------------------------------------------------------
# Library
#----------------------------------------------------------------------------

use CoralCommon v*
use Frontier_Client v* LCG_Interfaces

apply_pattern lcg_module_library

# Create a symlink .dylib -> .so on mac (sr #141482 - also see bug #37371)
apply_pattern lcg_dylib_symlink

#----------------------------------------------------------------------------
# Tests and utilities
#----------------------------------------------------------------------------

use CppUnit v* LCG_Interfaces -no_auto_imports

# The unit tests
apply_pattern coral_unit_test tname=CmsNewFrontier
apply_pattern coral_unit_test tname=MonitorController
apply_pattern coral_unit_test tname=MultipleSchemas
apply_pattern coral_unit_test tname=Schema
apply_pattern coral_unit_test tname=SegFault
apply_pattern coral_unit_test tname=SimpleQueries
apply_pattern coral_unit_test tname=TestAlias

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
