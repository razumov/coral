package CoralKernel

#============================================================================
# Public dependencies and build rules
#============================================================================

use CoralBase v*

apply_pattern lcg_shared_library

# Create a symlink .dylib -> .so on mac (sr #141482 - also see bug #37371)
apply_pattern lcg_dylib_symlink

#============================================================================
# Private dependencies and build rules
#============================================================================

private

macro_append lcg_CoralKernel_use_linkopts " -ldl "

use CppUnit v* LCG_Interfaces -no_auto_imports

macro_prepend CppUnit_linkopts " -Wl,--no-as-needed " target-mac ""
macro_append CppUnit_linkopts " -Wl,--as-needed " target-mac ""

# The unit tests
apply_pattern coral_unit_test tname=CoralKernelTest
apply_pattern coral_unit_test tname=ExternalPluginManager
apply_pattern coral_unit_test tname=PluginList
apply_pattern coral_unit_test tname=PluginManager
apply_pattern coral_unit_test tname=PropertyManager

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"
