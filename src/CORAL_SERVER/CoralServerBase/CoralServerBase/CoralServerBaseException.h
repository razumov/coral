#ifndef CORALSERVERBASE_CORALSERVERBASEEXCEPTION_H
#define CORALSERVERBASE_CORALSERVERBASEEXCEPTION_H 1

// Include files
#include "CoralBase/Exception.h"

namespace coral
{

  /** @class CoralServerBaseException
   *
   *  Base class for all exceptions thrown by CORAL_SERVER packages.
   *  See CORALCOOL-1522.
   *
   *  @author Andrea Valassi
   *  @date   2016-08-19
   */

  class CoralServerBaseException : public Exception
  {

  public:

    /// Constructor
    CoralServerBaseException( const std::string& message,
                              const std::string& method,
                              const std::string& module )
      : Exception( message, method, module ){}

    /// Destructor
    virtual ~CoralServerBaseException() throw() {}

  };

}
#endif // CORALSERVERBASE_CORALSERVERBASEEXCEPTION_H
