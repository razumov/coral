#ifndef CORALSERVERBASE_CALOPCODEEXCEPTION_H
#define CORALSERVERBASE_CALOPCODEEXCEPTION_H 1

// Include files
#include "CoralServerBase/CoralServerBaseException.h"

namespace coral
{

  /** @class CALOpcodeException
   *
   *  Exception thrown when manipulating CAL opcodes.
   *
   *  @author Andrea Valassi
   *  @date   2009-01-23
   *///

  class CALOpcodeException : public CoralServerBaseException
  {

  public:

    /// Constructor
    CALOpcodeException( const std::string& message,
                        const std::string& method )
      : CoralServerBaseException( message, method, "coral::CoralServerBase" ){}

    /// Destructor
    virtual ~CALOpcodeException() throw() {}

  };

}
#endif // CORALSERVERBASE_CALOPCODEEXCEPTION_H
