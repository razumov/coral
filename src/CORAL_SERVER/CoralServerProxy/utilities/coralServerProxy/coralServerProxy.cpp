//--------------------------------------------------------------------------
// File and Version Information:
//  $Id: coralServerProxy.cpp,v 1.1.2.4.2.2 2012-07-03 12:32:11 avalassi Exp $
//
// Description:
//  CoralProxy application
//
// Environment:
//  This software was developed for the ATLAS collaboration.
//
// Author List:
//  Andy salnikov
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <stdlib.h>
#include <unistd.h>
#include <errno.h>
#include <sys/types.h>
#include <sys/stat.h>
#include <fcntl.h>
#include <iostream>
#include <cstring>
#include <string>
#include <vector>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoralBase/MessageStream.h"
#include "CoralBase/../src/MsgReporter2.h"
#include "src/PacketCacheMemory.h"
#include "src/TPCManager.h"
#include "src/NetEndpointAddress.h"
#include "src/MsgLogger.h"
#include "src/StatCollector.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using std::cout ;
using std::cerr ;
using std::endl ;
using std::string ;
using std::vector ;

using namespace coral::CoralServerProxy ;

namespace {

  NetPort listen_port(40007);
  std::string pmap_lock_dir;
  bool override_portmap = false;
  bool unregister_portmap = true;
  NetPort server_port(40007);
  size_t maxCacheSizeMB = 1024 ;
  size_t maxCachePackets = 100000 ;
  size_t queue_size = 100000 ;
  unsigned timeout = 180 ;
  unsigned statSize = 10000 ;
  unsigned statAutoResetSec = 600 ;
  unsigned maxAccepErrors = 20 ;

  void
  usage( const char* argv0, std::ostream& out = cout )
  {
    out << "Usage: " << argv0 << " [options] server-host ...\n"
        << "  Available options:\n"
        << "    -h           - print help message and exit\n"
        << "    -i address   - interface to listen on [def: all]\n"
        << "    -p number    - listening port number [def: " << listen_port << "]\n"
        << "    -X path      - directory for port mapper lock file\n"
        << "    -P           - override existing port mapper registration\n"
        << "    -U           - do not unregister from port mapper on exit\n"
        << "    -s number    - server port number [def: " << server_port << "]\n"
        << "    -z number    - max packet queue size [def: " << queue_size << "]\n"
        << "    -c number    - max cache size in MB [def: " << maxCacheSizeMB << "]\n"
        << "    -k number    - max packet number in cache [def: " << maxCachePackets << "]\n"
        << "    -t seconds   - max timeout value [def: " << timeout << "]\n"
        << "    -x           - collect data processing statistics\n"
        << "    -y number    - statistics sample size [def: " << statSize << "]\n"
        << "    -a seconds   - statistics auto-reset interval [def: " << statAutoResetSec << "]\n"
        << "    -d           - start as a daemon\n"
        << "    -l path      - log file name\n"
        << "    -m           - (deprecated) allocate thread for connection manager\n"
        << "    -C           - close upstream connection when last client disconnects\n"
        << "    -e number    - max number of accept error before stop [def: " << maxAccepErrors << "]\n"
        << "    -v/q         - increase/decrease verbosity (can use many times)\n"
        << "\n"
        << " `server-host` is a comma-separated list of server endpoints, each endpoint\n"
        << "is specified as a host name or IPv4 address optionally followed by colon and\n"
        << "port number. If port is given then it overrides port number specified by -s\n"
        << "option. More than one `server-host` argument is acceptable, to the same\n"
        << "effect as if they were separated by commas.\n"
      ;
  }

  // close and open standard file descriptors
  int
  reOpenStd( const char* log )
  {
    int fdlog = -1 ;
    if ( log ) {
      // try to open log file first, if can't then return failure
      fdlog = open ( log, O_WRONLY | O_CREAT | O_TRUNC, 0666 ) ;
      if ( fdlog < 0 ) {
        cerr << "Failed to open the log file \"" << log << "\"" << endl ;
        return errno ;
      }
    }

    // close std files
    close ( 0 );
    if ( fdlog > 0 ) {
      close ( 1 );
      close ( 2 );
    }

    // open the files
    int fd0 = open ( "/dev/null", O_RDONLY ) ; // stdin
    if ( fd0 != 0 ) // Fix Coverity CHECKED_RETURN
      std::cerr << "WARNING! Failed to open /dev/null as 0" << std::endl ;

    if ( fdlog > 0 )
    {
      int fd1 = dup ( fdlog ) ; // stdout
      int fd2 = dup ( fdlog ); // stderr
      if ( fd1 != 1 ) // Fix Coverity RESOURCE_LEAK
        std::cerr << "WARNING! Failed to open logfile as 1" << std::endl ;
      if ( fd2 != 2 ) // Fix Coverity RESOURCE_LEAK
        std::cerr << "WARNING! Failed to open logfile as 2" << std::endl ;
    }

    if ( fdlog >= 0 ) close ( fdlog );

    return 0;
  }

  // daemonize process
  int
  daemonize()
  {
    // first fork
    pid_t pid = ::fork() ;
    if ( pid < 0 ) {
      // fork failed
      return errno ;
    } else if ( pid > 0 ) {
      // parent exits
      exit ( 0 ) ;
    }

    // Become the leader of a new session
    pid = ::setsid();
    if ( pid < 0 ) {
      // setsid failed
      return errno ;
    }

    // second fork
    pid = ::fork() ;
    if ( pid < 0 ) {
      // fork failed
      return errno ;
    } else if ( pid > 0 ) {
      // parent exits
      exit ( 0 ) ;
    }

    // do not lock current directory
    chdir ( "/" ) ;

    return 0 ;
  }

}

int
main( int argc, char** argv )
{
  // all command options
  bool daemon = false ;
  unsigned int verbosity = 2 ;
  const char* iface = 0 ;
  const char* log = 0 ;
  bool statCollect = false ;
  bool keepUpstreamOpen = true;

  // parse the options
  int c ;
  while ( ( c = getopt ( argc, argv, "hi:p:X:PUs:z:c:k:t:dxy:a:l:mCe:vq" ) ) != -1 ) {
    switch ( c ) {
    case 'h':
      usage ( argv[0], cout ) ;
      return 0 ;
    case 'i':
      iface = optarg ;
      break ;
    case 'p':
      listen_port = NetPort::parse(optarg);
      if (! listen_port.isValid()) {
        cerr << "Failed to parse -p option value: '" << optarg << "'\n";
        return 1 ;
      }
      break ;
    case 'X':
      pmap_lock_dir = optarg;
      break ;
    case 'P':
      override_portmap = true;
      break ;
    case 'U':
      unregister_portmap = false;
      break ;
    case 's':
      server_port = NetPort::parse(optarg);
      if (! server_port.isValid()) {
        cerr << "Failed to parse -s option value: '" << optarg << "'\n";
        return 1 ;
      }
      break ;
    case 'z':
      queue_size = atoi ( optarg ) ;
      break ;
    case 'c':
      maxCacheSizeMB = atoi ( optarg ) ;
      break ;
    case 'k':
      maxCachePackets = atoi ( optarg ) ;
      break ;
    case 't':
      timeout = atoi ( optarg ) ;
      break ;
    case 'x':
      statCollect = true ;
      break ;
    case 'y':
      statSize = atoi ( optarg ) ;
      break ;
    case 'a':
      statAutoResetSec = atoi ( optarg ) ;
      break ;
    case 'd':
      daemon = true ;
      break ;
    case 'l':
      log = optarg ;
      break ;
    case 'v':
      verbosity ++ ;
      break ;
    case 'q':
      if ( verbosity > 0 ) verbosity -- ;
      break ;
    case 'm':
      break ;
    case 'C':
        keepUpstreamOpen = false;
      break ;
    case 'e':
      maxAccepErrors = atoi ( optarg ) ;
      break ;
    case '?':
    default:
      usage ( argv[0], cerr ) ;
    return 1 ;
    }
  }

  // should have exactly one positional argument
  if ( argc == optind ) {
    cerr << "one or more positional arguments required\n" ;
    usage ( argv[0], cerr ) ;
    return 1 ;
  }

  setenv("CORAL_MSGFORMAT", "ATLAS", 1);
  //install and replace the old message reporter
  coral::MessageStream::installMsgReporter(new coral::CoralBase::MsgReporter2);

  coral::MsgLevel levels[] = {coral::Error, coral::Warning, coral::Info,
      coral::Debug, coral::Verbose};
  unsigned nLevels = sizeof levels / sizeof levels[0];
  if (verbosity >= nLevels) verbosity = nLevels - 1;
  coral::MessageStream::setMsgVerbosity(levels[verbosity]);

  // get server endpoints
  vector<string> server_endpoints;
  for (int opt = optind; opt != argc; ++ opt) {
    string const endpoints(argv[opt]);

    string::size_type p = 0;
    while (true) {

      string::size_type const p1 = endpoints.find(',', p);
      string endpoint(endpoints, p, p1-p);

      // if hostname starts with "env:" then get it from the envvar that follows ':'
      if ( endpoint.compare(0, 4, "env:") == 0 ) {
        endpoint.erase(0, 4);
        string port;
        string::size_type const p2 = endpoint.find(':');
        if (p2 != string::npos) {
          port = endpoint.substr(p2);
          endpoint.erase(p2);
        }

        const char* server_host = getenv ( endpoint.c_str() ) ;
        if ( ! server_host ) {
          cerr << "Environment variable \"" << endpoint << "\" is not defined\n" ;
          return 1 ;
        }
        endpoint = server_host + port;
      }

      server_endpoints.push_back(endpoint);

      if (p1 == string::npos) break;

      p = p1 + 1;
    }
  }

  // resolve the addresses
  NetEndpointAddress iface_address ( listen_port ) ;
  vector<NetEndpointAddress> servers ;
  try {

    if ( iface ) iface_address = NetEndpointAddress ( iface, listen_port ) ;

    for (vector<string>::const_iterator it = server_endpoints.begin(); it != server_endpoints.end(); ++ it) {
      string endpoint = *it;
      NetPort port = server_port;
      string::size_type const p = endpoint.find(':');
      if (p != string::npos) {
        port = NetPort::parse(endpoint.substr(p+1).c_str());
        if (! port.isValid()) {
          cerr << "Invalid port number \"" << endpoint.substr(p+1) << "\"\n" ;
          return 1 ;
        }
        endpoint.erase(p);
      }
      servers.push_back(NetEndpointAddress(endpoint, port));
    }
  } catch ( const std::exception& e ) {
    cerr << e.what() << endl ;
    return 1 ;
  }

  // force log to /dev/null in case I'm daemon
  // and no log file was defined by user
  if ( daemon && ! log ) log = "/dev/null" ;

  // re-open standard file descriptors
  umask ( 022 ) ;
  int s = reOpenStd( log ) ;
  if ( s != 0 ) return s ;

  // daemonize if needed
  if ( daemon ) {
    int st = ::daemonize() ;
    if ( st != 0 ) {
      cerr << "failed to daemonize the process" << endl ;
      return st ;
    }
  }

  // instantiate cache instance
  PacketCacheMemory cache ( maxCacheSizeMB, maxCachePackets ) ;

  // statistics collection
  StatCollector* stat = 0 ;
  if ( statCollect ) {
    stat = new StatCollector ( statSize, statAutoResetSec ) ;
  }

  try { // Move everything inside try block to fix Coverity UNCAUGHT_EXCEPT

    // instantiate connection manager
    TPCManager manager ( iface_address, override_portmap, servers, queue_size,
                         timeout, cache, stat, keepUpstreamOpen, maxAccepErrors,
                         unregister_portmap, pmap_lock_dir) ;

    PXY_INFO ( "Give control to the connection manager" ) ;
    // run connection manager in the main thread, it will handle signals itself
    if (manager.run() < 0) {
      return 1;
    }

  }
  catch ( std::exception& e )
  {
    std::cerr << "C++ Exception : " << e.what() << std::endl;
    return 1;
  }
  catch ( ... )
  {
    std::cerr << "Unhandled exception " << std::endl;
    return 1;
  }

  return 0 ;
}
