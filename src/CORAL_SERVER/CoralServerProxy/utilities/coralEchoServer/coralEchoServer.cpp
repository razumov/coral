//--------------------------------------------------------------------------
// File and Version Information:
//  $Id: coralServerProxy.cpp,v 1.1.2.4.2.2 2012-07-03 12:32:11 avalassi Exp $
//
// Description:
//  Simple server application that echoes CORAL packets back to
//  client, only useful for testing.
//
// Environment:
//  This software was developed for the ATLAS collaboration.
//
// Author List:
//  Andy Salnikov
//
//------------------------------------------------------------------------

//-----------------
// C/C++ Headers --
//-----------------
#include <ctype.h>
#include <errno.h>
#include <iostream>
#include <netinet/in.h>
#include <netinet/tcp.h>
#include <string.h>
#include <sys/socket.h>
#include <time.h>
#include <unistd.h>

//-------------------------------
// Collaborating Class Headers --
//-------------------------------
#include "CoralBase/../src/coral_thread_headers.h"
#include "src/NetEndpointAddress.h"
#include "src/NetSocket.h"
#include "src/MsgLogger.h"
#include "src/Packet.h"

//-----------------------------------------------------------------------
// Local Macros, Typedefs, Structures, Unions and Forward Declarations --
//-----------------------------------------------------------------------

using std::cout;
using std::cerr;
using std::endl;

using namespace coral::CoralServerProxy;

namespace {

NetPort listen_port(40007);
std::string pmap_lock_dir;
unsigned delay_ms = 0;
bool delay_payload = false;

void usage(const char* argv0, std::ostream& out = cout)
{
    out << "Usage: " << argv0 << " [options]\n"
        << "  Available options:\n"
        << "    -h              - print help message and exit\n"
        << "    -i address      - interface to listen on [def: all]\n"
        << "    -p number       - listening port number [def: " << listen_port << "]\n"
        << "    -X path         - directory for port mapper lock file\n"
        << "    -d milliseconds - global response delay in milliseconds [def: " << delay_ms << "]\n"
        << "    -D              - use per-request delay from payload [def: no]\n"
        << "    -v/q            - increase/decrease verbosity (can use many times)\n";
}

NetSocket setupListener(const NetEndpointAddress& listenAddress);

class Echo {
public:
    Echo(NetSocket const& socket)
                    : m_socket(socket)
    {
    }
    void operator()();
    void delay(const std::string& payload);
private:
    NetSocket m_socket;
};

}

int main(int argc, char** argv)
{
    // all command options
    unsigned int verbosity = 2;
    const char* iface = 0;

    // parse the options
    int c;
    while ((c = getopt(argc, argv, "hi:p:X:d:Dvq")) != -1) {
        switch (c) {
        case 'h':
            usage(argv[0], cout);
            return 0;
        case 'i':
            iface = optarg;
            break;
        case 'p':
            ::listen_port = NetPort::parse(optarg);
            if (! listen_port.isValid()) {
              cerr << "Failed to parse -p option value: '" << optarg << "'\n";
              return 1 ;
            }
            break;
        case 'X':
            pmap_lock_dir = optarg;
            break ;
        case 'd':
            ::delay_ms = atoi(optarg);
            break;
        case 'D':
            ::delay_payload = true;
            break;
        case 'v':
            verbosity++;
            break;
        case 'q':
            if (verbosity > 0)
                verbosity--;
            break;
        case '?':
        default:
            usage(argv[0], cerr);
            return 1;
        }
    }

    // should have no positional arguments
    if (argc - optind != 0) {
        cerr << "unexpected positional arguments\n";
        usage(argv[0], cerr);
        return 1;
    }

    coral::MsgLevel levels[] = {coral::Error, coral::Warning, coral::Info,
        coral::Debug, coral::Verbose};
    unsigned nLevels = sizeof levels / sizeof levels[0];
    if (verbosity >= nLevels) verbosity = nLevels - 1;
    coral::MessageStream::setMsgVerbosity(levels[verbosity]);

    // resolve the addresses
    NetEndpointAddress iface_address;
    try {
        if (iface) {
            iface_address = NetEndpointAddress(iface, ::listen_port);
        } else {
            iface_address = NetEndpointAddress(::listen_port);
        }
    } catch (const std::exception& e) {
        cerr << e.what() << endl;
        return 1;
    }

    try {

        NetSocket sock = setupListener(iface_address);
        if (not sock.isOpen())
            return 1;

        while (true) {

            // accept new connections
            NetSocket cli_sock(sock.accept());
            if (not cli_sock.isOpen()) {
                PXY_ERR("accept failed: " << strerror(errno));
            } else {
                PXY_INFO("new client connection " << cli_sock << " from " << cli_sock.peer());
            }

            // start new thread to server one socket
            coral::thread thread((Echo(cli_sock)));

            // detach and let it run freely
            thread.detach();

        }

    } catch (std::exception& e) {
        std::cerr << "C++ Exception : " << e.what() << std::endl;
        return 1;
    } catch (...) {
        std::cerr << "Unhandled exception " << std::endl;
        return 1;
    }

    return 0;
}

namespace {

int setSocketOptions(NetSocket& sock)
{
    // reuse the address
    if (sock.setSocketOptions(SOL_SOCKET, SO_REUSEADDR, 1) < 0) {
        return -1;
    }
    if (sock.setSocketOptions(SOL_SOCKET, SO_KEEPALIVE, 1) < 0) {
        return -1;
    }
    if (sock.setSocketOptions(IPPROTO_TCP, TCP_NODELAY, 1) < 0) {
        return -1;
    }
    return 0;
}

NetSocket setupListener(const NetEndpointAddress& listenAddress)
{
    // create socket
    PXY_DEBUG("Creating listening socket");
    NetSocket sock(PF_INET, SOCK_STREAM, 0);
    if (not sock.isOpen()) {
        PXY_ERR("Failed to create a socket: " << strerror(errno));
        return sock;
    }

    // set socket options
    if (setSocketOptions(sock) < 0) {
        PXY_ERR("Failed to set socket options " << sock << ": " << strerror(errno));
        return NetSocket();
    }

    // bind the socket
    PXY_DEBUG("Binding socket " << sock);
    if (sock.bind(listenAddress) < 0) {
        PXY_ERR("Failed to bind the socket: " << strerror(errno));
        return NetSocket();
    }

    // register port with portmapper if needed
    unsigned short port = sock.local().port().port();
    if (listenAddress.registerPort(port, false, pmap_lock_dir)) {
      PXY_INFO("Registered port with portmapper " << port << " -> " << listenAddress.port());
    } else {
      PXY_ERR("Failed to registered port with portmapper");
      return NetSocket() ;
    }

    // listen on this socket
    PXY_INFO("Listening on " << port << ", " << sock << ", maxconn=" << SOMAXCONN);
    if (sock.listen(SOMAXCONN) < 0) {
        PXY_ERR("Failed to listen the socket: " << strerror(errno));
        return NetSocket();
    }

    return sock;
}


// sleep for a little while based on delay specified in payload or
// in global settings
void Echo::delay(const std::string& payload)
{
    unsigned delay = ::delay_ms;

    // try to find delay value in a message
    if (::delay_payload) {
        static std::string const delay_spec = "DELAY_MS=";
        std::string::size_type pos = payload.find(delay_spec);
        if (pos != std::string::npos) {
            std::string::size_type p1 = pos + delay_spec.size();
            delay = strtol(payload.c_str()+p1, 0, 10);
        }
    }

    if (delay) {
        PXY_INFO(m_socket << ": delay for " << delay << " ms");
        timespec ts = {delay / 1000, (delay % 1000) * 1000000};
        nanosleep(&ts, &ts);
    }
}

void Echo::operator()()
{
    // the code below does not throw, no need to catch exceptions

    while (true) {

        // read a packet
        PacketPtr p = Packet::read(m_socket, Packet::Request);
        if (not p) {
            PXY_INFO(m_socket << ": client closed connection");
            break;
        }
        PXY_INFO(m_socket << ": received new packet " << *p);

        std::string payload((const char*)p->data(), p->dataSize());

        // possibly some delay here
        delay(payload);

        // build response packet
        const coral::CALPacketHeader& calHeader = p->calHeader();
        PacketPtr reply = Packet::buildReply(calHeader.opcode(),
                                             p->ctlHeader(),
                                             payload,
                                             calHeader.cacheable());

        // send reply
        int bytes = reply->write(m_socket);
        if (bytes == 0) {
            PXY_ERR(m_socket << ": EOF while writing to socket, will close connection");
            break;
        } else if (bytes < 0) {
            PXY_ERR(m_socket << ": error while writing to socket: " << strerror(errno));
            break;
        }
    }

    m_socket.shutdown();
    m_socket.close();
}

}
