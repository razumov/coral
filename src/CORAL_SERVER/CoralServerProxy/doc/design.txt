
             == Main components of CoralServerProxy ==

- main thread running thread manager (currently thread-per-connection 
  manager) [class TPCManager]
- client reader threads (currently one thread per client connection) 
  [class SingleClientReader]
- client writer thread [class ClientWriter]
- server reader thread [class ServerReader]
- server writer thread [class serverWriter]
- dispatcher thread [class PacketDispatcher]
- three packet queues [classes PacketQueue and PacketHeaderQueue]
  - receive queue for all incoming packets (either from clients, server, 
    or internal control packets)
  - server queue for packets to be sent to server
  - client queue for packets to be sent to clients
- connection manager which registers all client connections [class 
  ClientConnManager]
- packet cache [class PacketCacheMemory]
- packet routing table [class RoutingTables]



                  == TPCManager functions ==

This is the main loop responsible for all initialization, accepting 
connections from clients and instantiating almost all threads.

Initialization:
- opens listening socket, stop immediately if fails
- instantiates all three packet queues
- instantiates routing table
- instantiates connection manager
- starts dispatcher thread
- starts client writer thread
- starts server writer thread

Main loop:
- accepts new connection
- tries to add new connection to connection manager, if fails closes the 
  connection (failure usually means that server connection cannot be opened)
- if succeeds creates new client reader thread

Main loop runs forever, potentially if something bad could happen to 
listening socket accept() may start failing which all the time. Might want 
to add a detection of such failures and terminate main loop when this 
happens.


               == SingleClientReader functions ==

SingleClientReader is responsible for receiving packets from clients and 
queuing them for processing. It also handles client disconnects.

When constructed instance of this class receives following objects:
- socket to read data from
- reference to packet queue
- reference to routing table
- reference to connection manager

All code runs in internal loop:
- reads whole packet from a socket, if read fails stop the loop
- verifies that first packet to arrive has a ConnectRO opcode 
  - if not then builds a reply packet and sends it to the same socket, 
    then stops the loop
  - this does not play well if client re-connects to proxy in the middle 
    of data exchange
- gets client id from a packet, finds corresponding server id from routing 
  table and overrides ID in a packet
- sets "from proxy" flag in a packet
- tries to push packet into a queue, if timeout happens stop the loop
 
When the loop stops:
- gets the list of server-side IDs that this client has genereated
- for every id bulds "client disconnect" control packet and pushes it to 
  a queue
  - if timeout happens just forget about it
- removes socket (and all corresponding ids from routing table)
- removes socket from connection manager
- closes socket
- finishes


                 == ClientWriter functions ==

ClientWriter is responsible for sending packets to clients.

When constructed instance of this class receives following objects:
- reference to packet queue
- reference to routing table
- reference to connection manager

Everything runs in an internal loop:
- gets next packet from packet queue (hangs forever if there is nothing)
- control packets are discarded now (the code which closes all connections 
  registered in connection manager is commented out)
- get server-side clientID and translates it into (socket, clientID) using 
  the routing table
- builds new header object, send header and packet payload to the socket
- if error happens during the write prints error message but does nothing


                 == ServerReader functions ==

Server reader is responsible for getting packets from server and sending 
them to receive queue.

When constructed instance of this class receives following objects:
- reference to packet queue
- reference to connection manager

All code runs in internal loop:
- gets server socket from connection manager
- reads whole packet from a socket, if read fails stop the loop
- sets "from proxy" flag in a packet
- tries to push packet into a queue, if timeout happens stop the loop
 
When the loop stops:
- builds "server shutdown" control packet and pushes it to a queue
  - if timeout happens just forget about it
- closes socket
- finishes


                 == ServerWriter functions ==

ServerWriter is responsible for sending server-directed packets to the 
server socket.

When constructed instance of this class receives following objects:
- reference to packet queue
- reference server socket

All code runs in internal loop:
- gets next packet from packet queue (hangs forever if there is nothing)
- control packets are discarded
- sends packet to the socket
- if error happens print a message and continue

The loop never exits, as it holds socket by reference some other thread 
can update it and change its state.


                 == PacketDispatcher functions ==

PacketDispatcher is responsible for sending incoming packets to correct 
destination.

When constructed instance of this class receives following objects:
- references to all three packet queues
- reference to packet cache instance

PacketDispatcher has few internal structures:
- queued packet list, list of packets that were sent to server and for 
  which we expect response but have not received it yet
- multi-part collector, collects pieces of packets if packets are sent 
  in several pieces

All code runs in internal loop:
- gets next packet from receive queue (hangs forever if there is nothing)
- if the packet type is Request (comes from client):
  - special reserved opcodes are processed by dispatcher itself (statistics)
  - for transaction-related requests send OK response immediately
  - StartTransactionRW causes immediate exception response packet
  - ConnectRO packets are processed as regular packets (except for some very 
    special packets)
  - multi-segment requests are moved to server queue (no caching for them)
  - if cacheable flag is set in packet:
    - if found in cache then send cached response to client queue
    - if identical packet is in the queued packet list then add this request 
      to the queued list too (some complications due to multi-part responses)
    - otherwise add it to the queued list and server queue
  - non-cacheable packets are sent directly to server queue
- if the packet type is Reply (came from server):
  - if corresponding request is not in the queued packet list then send 
    reply to client queue directly
  - otherwise send the reply for every matching request in the queued packet 
    list
    - if packet is last part of multi-part then store it (and request) 
      in cache
    - remove matching requests from queued list if reply is complete
- if packet type is Control (internally-generated packet):
  - if opcode is ServerShutdown then clear queued packet list and 
    multi-part collector
  - if opcode is ClientDisconnect then remove packets from that client from 
    queued packet list
  - all control packets are also forwarded to both server queue and client 
    queue


                 == ClientconnManager functions ==

ClientConnManager manages all client (and server connections. For server 
connection is uses helper class ServerReaderFactory.

ServerReaderFactory has a single useful method serverConnect():
- creates and connects new socket to server
- if socket is OK then start new thread running ServerReader instance
- returns socket object

ClientConnManager has following data members:
- ServerReaderFactory instance
- server socket
- list of client sockets

ClientConnManager has several methods (all methods are thread-safe):
- addConnection(socket):
  - adds new socket to the list of client sockets
  - if server socket is closed then calls serverConnect() on server reader 
    factory
- removeConnection(socket):
  - removes socket from client socket list, closing it is somebody else's 
    responsibility
- serverConnection():
  - returns a reference to a server socket (may be closed)
- setServerConnection(sock):
  - updates server socket
- closeAllConnections():
  - calls shutdown() on every client socket and server socket
  - clears client socket list

ClientConnManager methods are called from these locations:
- addConnection():
  - called by TPCManager when new client connection is accepted but before 
    new client thread is started
- removeConnection():
  - called by SingleClientReader right before reader thread exits
- serverConnection():
  - called from ServerReader before the event loop
  - called by TCPManager which passes socket reference to ServerWriter
- setServerConnection():
  - called by ServerReaderFactory when new server connection is established


                 == RoutingTables functions ==

RoutingTables implements bi-directional mapping from (clientID, socket) to 
serverID. Used by client readers and writer to provide unique client ID for 
packets that are multiplexed from several client connections into a single 
server connection. 


                == Trouble With Disconnects ==

Because of the inherent parallelism of the proxy architecture some conditions 
are very difficult to handle and they may need more syncronization than there 
is currently in the system. The control packets that were introduced for 
messaging inside proxy handle asynchronous communications relatively OK but 
they do not apparently work well for things that need to be done synchronously.

In initial design I tried to implement server-side disconnects when proxy 
closes last client connection. This was implemented using control packets 
(there are still traces of commented-out code):
- when connection manager notices inside removeConnection() that the last 
  client socket is being closed then it would also call shutdown() on server 
  socket
- this would lead to error in server reader which would generate 
  ServerShutdown control packet
- ServerShutdown packet would get to dispatcher and cause cleanup of various 
  structures
- optionally ServerShutdown would be detected by client writer which will 
  close remaining connections (if any)
Trouble with this logic is the race condition between moving ServerShutdown 
packet around and other activities (new clients connecting).

One possible way to solve this is when server disconnect happens or when we 
decide to disconnect the server ourselves we need:
1. to stop accepting new client connections
2. close all existing connections to clients and server
3. reset the system to clean state
4. start accepting connections again

Resetting the system could be done in at least two ways:
1. clean all queues and all internal state of every component
  a. wait until all reader threads have finished so that they don't fill up 
    queues
  b. remove everything from queues or wait until threads consume everything 
    from them
  c. reset state of dispatcher, connection manager and routing table
2. rebuild whole shebang from scratch:
  a. stop all threads
    i. reader threads will stop by themselves when their sockets are shutdown
    ii. writer threads and dispatcher need to be terminated
  b. delete all queues and components
  c. re-do initialization, what happens in TPCManager class except for 
    re-opening listening socket (there may be incoming connections which we 
    do not want to abort prematurely)


                == Cleanup implementation ==

Here is the description of what has been changed to implement cleanup after 
server disconnect. One bonus feature that has been added in this 
implementation is proxy-initiated server disconnect when last client goes 
away. As mentioned above this feature was planned from the very beginning 
but its initial implementation suffered from race condition.

From the above two options for cleanup I chose option 1, that is resetting 
all components without stopping all threads. Reader threads are stopped 
because their corresponding connections are closed and reader threads do not 
make sense without connection.

One new class is introduced - ClientReaderFactory which is a factory for 
client reader threads.
- single method readerThread() creates new client reader thread and returns 
  shared pointer for this thread

Class ServerReaderFactory:
- its single method serverConnect() was split into two:
  - serverConnect() which returns new socket for server connection
  - readerThread() which returns shared pointer for a new server reader thread

ClientConnManager class is now a central place where all cleanup happens. It 
has references to almost all other components of the proxy and calls their 
cleanup methods when appropriate.

There are two cases when cleanup is performed:
1. When last client connection goes away
  a. client thread calls ClientConnManager::removeConnection() which is 
    synchronized so that new connections do not happen until this method
    completes 
  b. ClientConnManager closes server connection (some precautions to avoid 
    race condition are needed there)
  c. server thread notices that connection was closed by proxy (NetSocket is 
    in closed state) and simply finishes
  d. ClientConnManager waits for server reader thread to finish (joins it)
  e. ClientConnManager clears all queues and routing tables
  f. ClientConnManager adds ServerShutdown packet to receive queue
  g. Dispatcher thread reads ServerShutdown packet and resets its internal 
    state
2. When server connection goes away
  a. server reader thread notices this as an error when reading data from 
    socket
  b. the socket is not closed yet so server thread calls 
    ClientConnManager::closeAllConnections() which is synchronized so that 
    new connections do not happen until this method completes
  c. ClientConnManager calls shutdown on every open client socket (they are 
    closed by their corresponding threads)
  d. ClientConnManager calls shutdown and close on server socket
  e. ClientConnManager waits until all client threads and server thread 
    finish (except for a thread which runs this code)
  f. ClientConnManager clears its own state (list of client sockets and 
    threads)
  g. ClientConnManager clears all queues and routing tables
  h. ClientConnManager adds ServerShutdown packet to receive queue
  i. Dispatcher thread reads ServerShutdown packet and resets its internal 
    state
