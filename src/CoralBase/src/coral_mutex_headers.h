#ifndef CORALBASE_CORALMUTEXHEADERS_H
#define CORALBASE_CORALMUTEXHEADERS_H 1

// Include files
#include <condition_variable>
#include <mutex>

// Typedef coral::mutex for CORAL internals (task #50199)
namespace coral
{
  typedef std::condition_variable          condition_variable;
  typedef std::mutex                       mutex;
  typedef std::lock_guard<std::mutex>      lock_guard;
  typedef std::unique_lock<std::mutex>     unique_lock;
  typedef std::timed_mutex                 timed_mutex;
}

#endif // CORALBASE_CORALMUTEXHEADERS_H
