#if PY_MAJOR_VERSION >= 3
    #define PyString_Check PyUnicode_Check
    char* PyString_AsString(PyObject* string)
    {
        return const_cast<char*>(PyUnicode_AsUTF8(string));
    }
    #define PyString_AS_STRING PyString_AsString
#endif
