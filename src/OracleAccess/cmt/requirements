package OracleAccess

#============================================================================
# Private dependencies and build rules
#============================================================================

private

# Temporary hack to avoid c++ deprecation warnings for auto_ptr (bug #98085)
macro_append cppflags "" target-c11 " -Wno-deprecated "

#----------------------------------------------------------------------------
# Library
#----------------------------------------------------------------------------

use CoralCommon v*
use oracle * LCG_Interfaces

apply_pattern lcg_module_library

# Create a symlink .dylib -> .so on mac (sr #141482 - also see bug #37371)
apply_pattern lcg_dylib_symlink

#----------------------------------------------------------------------------
# Tests and utilities
#----------------------------------------------------------------------------

use CppUnit v* LCG_Interfaces -no_auto_imports

# The unit tests
apply_pattern coral_unit_test tname=BulkInserts
apply_pattern coral_unit_test tname=Connection
apply_pattern coral_unit_test tname=DataEditor
apply_pattern coral_unit_test tname=DataDictionary
apply_pattern coral_unit_test tname=DateAndTime
apply_pattern coral_unit_test tname=Dual
apply_pattern coral_unit_test tname=GroupBy
apply_pattern coral_unit_test tname=MultipleSchemas
apply_pattern coral_unit_test tname=MultipleSessions
apply_pattern coral_unit_test tname=MultiThreading
apply_pattern coral_unit_test tname=Schema
apply_pattern coral_unit_test tname=SimpleQueries
apply_pattern coral_unit_test tname=Views

# Fake target for utilities
action utilities "echo No utilities in this package"
macro_remove cmt_actions_constituents "utilities"

# Fake target for examples
action examples "echo No examples in this package"
macro_remove cmt_actions_constituents "examples"

#----------------------------------------------------------------------------
# Workarounds for LCGCMT issues on MacOSX
#----------------------------------------------------------------------------

# === Workaround in both CoralTest and OracleAccess ===
# Workaround for SPI-787 (using a wrong version of the oracle client)
macro oracle_config_version $(oracle_config_version) target-mac 11.2.0.3.0
macro oracle_home $(oracle_home) target-mac /afs/cern.ch/sw/lcg/external/oracle/$(oracle_native_version)/macosx64

