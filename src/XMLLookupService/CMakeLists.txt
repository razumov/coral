# Required external packages
find_package(XercesC REQUIRED)
include_directories(${XERCESC_INCLUDE_DIRS})

include(CORALModule)
CORALModule(LIBS lcg_CoralCommon ${XERCESC_LIBRARIES}
            TESTS LoadService)

# Install the dblookup.xml for tests (CORALCOOL-897)
include(CORALConfigScripts)
copy_and_install_program(dblookup.xml tests/LoadService tests/bin/XMLLookupService/LoadService tests/bin/XMLLookupService/LoadService)
copy_and_install_program(dblookup1.xml tests/LoadService tests/bin/XMLLookupService/LoadService tests/bin/XMLLookupService/LoadService)
copy_and_install_program(dblookup2.xml tests/LoadService tests/bin/XMLLookupService/LoadService tests/bin/XMLLookupService/LoadService)
